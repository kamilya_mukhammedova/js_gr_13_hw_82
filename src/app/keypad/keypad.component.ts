import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { addChar, removeChar } from '../keypad.actions';
import { KeypadData } from '../model/keypad.model';

@Component({
  selector: 'app-keypad',
  templateUrl: './keypad.component.html',
  styleUrls: ['./keypad.component.css']
})
export class KeypadComponent {
  keypad!: Observable<KeypadData>;
  isClicked = false;
  stars = '';

  constructor(private store: Store<{ keypad: KeypadData }>) {
    this.keypad = store.select('keypad');
  }

  onAddChar(char: string) {
    this.store.dispatch(addChar({char: char}));
    this.isClicked = false;
  }

  onRemoveChar() {
    this.store.dispatch(removeChar());
    this.isClicked = false;
  }

  changeChar(char: string | undefined) {
    this.stars = '';
    for (let i = 0; i < char!.length; i++) {
      this.stars += '*';
    }
    return this.stars;
  }

  onCheck() {
    this.isClicked = true;
  }
}
