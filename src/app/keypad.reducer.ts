import { createReducer, on } from '@ngrx/store';
import { addChar, removeChar } from './keypad.actions';

const correctPassword = '1337';

const initialState = {
  isCorrect: false,
  password: '',
};

export const keypadReducer = createReducer(
  initialState,
  on(addChar, (state, {char}) => {
    if (state.password.length >= 4) {
      return state;
    }
    const newPassword = state.password + char;
    const newState = {...state, password: newPassword};
    if (newPassword === correctPassword) {
      newState.isCorrect = true;
    }
    return newState;
  }),
  on(removeChar, (state) => {
    const changedPassword = state.password;
    return {...state, password: changedPassword.substring(0, changedPassword.length - 1), isCorrect: false};
  })
);
