export interface KeypadData {
  isCorrect: boolean;
  password: string;
}
